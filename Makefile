
# scl enable devtoolset-7 -- make rpm upload

NAME=haproxy
BASE__PATH ?= $(shell pwd)
export RPMS_OUT = $(shell pwd)/release

rpm:
	rm -rf $(BASE__PATH)/cmake
	mkdir -p $(BASE__PATH)/cmake
	tar xf cmake-3.20.0-linux-x86_64.tar.gz -C $(BASE__PATH)/cmake --strip-components=1
	rm -rf bld/
	mkdir -p bld
	LD_PRELOAD=~/build-host/myhostname/lib64/libmyhostname_x86_64.so \
	PATH=$(BASE__PATH)/cmake/bin:$$PATH \
	scl enable devtoolset-7 -- \
	rpmbuild -bb \
	  --define '_topdir    $(PWD)/bld' \
		--define '_sourcedir $(PWD)' \
		--define '_rpmdir    $(RPMS_OUT)' \
		--define '_srcrpmdir $(RPMS_OUT)' \
		--define 'dist .el6' --define 'rhel 6' \
		$(NAME).spec


upload:
	jfrog rt upload "$(RPMS_OUT)/*/haproxy*rpm" sre-tooling/tools/6/x86_64/
	jfrog rt upload "$(RPMS_OUT)/*/haproxy*rpm" sre-tooling/tools/7/x86_64/
	jfrog rt upload "$(RPMS_OUT)/*/haproxy*rpm" sre-tooling/tools/8/x86_64/
	jfrog rt upload "$(RPMS_OUT)/*/haproxy*rpm" sre-upstream/haproxy/6/x86_64/
	jfrog rt upload "$(RPMS_OUT)/*/haproxy*rpm" sre-upstream/haproxy/7/x86_64/
	jfrog rt upload "$(RPMS_OUT)/*/haproxy*rpm" sre-upstream/haproxy/8/x86_64/

# diff -Naur src/http.c.orig src/http.c >/cvs/build/haproxy/error-pages-2.3.5.patch
