%define haproxy_user    haproxy
%define haproxy_uid     188
%define haproxy_group   haproxy
%define haproxy_gid     188
%define haproxy_home    %{_localstatedir}/lib/haproxy
%define haproxy_confdir %{_sysconfdir}/haproxy
%define haproxy_datadir %{_datadir}/haproxy

%define haproxy_lib     %{haproxy_home}
%define haproxy_mod     %{haproxy_home}/modules

%global _hardened_build 1

%{!?__global_ldflags: %global __global_ldflags -Wl,-z,relro}

%if 0%{?fedora} >= 19 || 0%{?rhel} >= 7
%bcond_without systemd
%else
%bcond_with systemd
%endif

%bcond_without lua

%define version   2.4.8
%define patchver  2.4.0
%define openssl_ver 1.1.1l
#%define dev_rel dev25
%define release 1

%define ot_cpp_ver       1.6.0
%define jaeger_cpp_ver   0.7.0
%define ot_c_wrapper_ver 1.1.0

Name: haproxy
Summary: HA-Proxy is a TCP/HTTP reverse proxy for high availability environments
Version: %{version}
Release: %{release}%{?dist}
License: GPLv2+
URL: http://www.haproxy.org/
Group: System Environment/Daemons

Source0: http://www.haproxy.org/download/2.3/src/haproxy-%{version}.tar.gz
Source1: haproxy.init
Source2: haproxy.cfg
Source3: haproxy.logrotate
Source4: haproxy.sysconfig
Source5: halog.1
Source6: haproxy.service
Source7: jaeger-config.json

Source10: lua-5.3.5.tar.gz
Source11: openssl-%{openssl_ver}.tar.gz
Source12: pcre-8.42.tar.bz2
Source14: opentracing-cpp-%{ot_cpp_ver}.tar.gz
Source15: opentracing-c-wrapper-%{ot_c_wrapper_ver}.tgz
Source16: jaeger-client-cpp-%{jaeger_cpp_ver}.tar.gz
Source20: http-errors.tar.gz

Patch0:   error-pages-%{patchver}.patch

Requires(pre): %{_sbindir}/groupadd
Requires(pre): %{_sbindir}/useradd
Requires(post): /sbin/chkconfig
Requires(preun): /sbin/chkconfig
Requires(preun): /sbin/service
Requires(postun): /sbin/service

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: zlib-devel
BuildRequires: readline-devel
BuildRequires: setup >= 2.5

Requires: zlib
Requires(pre):      shadow-utils
%if %{with systemd}
Requires(post):     systemd
Requires(preun):    systemd
Requires(postun):   systemd
BuildRequires:      systemd-units systemd-devel
%else
Requires(post):     chkconfig
Requires(preun):    chkconfig
Requires(preun):    initscripts
Requires(postun):   initscripts
%endif


%description
HAProxy is a free, fast and reliable solution offering high
availability, load balancing, and proxying for TCP and HTTP-based
applications. It is particularly suited for web sites crawling under
very high loads while needing persistence or Layer7 processing.
Supporting tens of thousands of connections is clearly realistic with
modern hardware. Its mode of operation makes integration with existing
architectures very easy and riskless, while still offering the
possibility not to expose fragile web servers to the net.

%prep
%setup -q -n %{name}-%{version}
%patch0 -p0

tar xf %{SOURCE20}
mv http-errors/*.http ./examples/errorfiles/

rm -rf $RPM_BUILD_DIR/libs
mkdir -p $RPM_BUILD_DIR/libs
tar xf %{SOURCE10} -C $RPM_BUILD_DIR/libs
tar xf %{SOURCE11} -C $RPM_BUILD_DIR/libs
tar xf %{SOURCE12} -C $RPM_BUILD_DIR/libs
tar xf %{SOURCE14} -C $RPM_BUILD_DIR/libs
tar xf %{SOURCE15} -C $RPM_BUILD_DIR/libs
tar xf %{SOURCE16} -C $RPM_BUILD_DIR/libs

%build

###############################################################################
### jaeger-client-cpp
###############################################################################
pushd $RPM_BUILD_DIR/libs/jaeger-client-cpp-%{jaeger_cpp_ver}
rm -rf build.d/
mkdir  build.d
cd     build.d

export CFLAGS="$CFLAGS -march=x86-64"
export CXXFLAGS="$CXXFLAGS -march=x86-64"

cat <<EOF > export.map
{
      global:
          OpenTracingMakeTracerFactory;
      local: *;
};
EOF

cmake \
       -DCMAKE_BUILD_TYPE=Release \
       -DJAEGERTRACING_PLUGIN=ON \
       -DJAEGERTRACING_WITH_YAML_CPP=OFF \
       -DJAEGERTRACING_BUILD_EXAMPLES=OFF \
       -DJAEGERTRACING_BUILD_CROSSDOCK=OFF \
       -DBUILD_TESTING=OFF \
       -DCMAKE_BUILD_TYPE=Release \
       -DYAML_BUILD_SHARED_LIBS=OFF \
       -DHUNTER_CONFIGURATION_TYPES=Release \
       -DHUNTER_STATUS_DEBUG=ON \
      ..
make %{?_smp_mflags}
popd
###############################################################################

pushd $RPM_BUILD_DIR/libs/opentracing-cpp-%{ot_cpp_ver}
  mkdir build.d 
  cd build.d
  cmake \
    -DCMAKE_CXX_FLAGS="-fPIC -Wl,--rpath=%{haproxy_lib} -Wl,--rpath-link=%{haproxy_lib}" \
    -DBUILD_TESTING=OFF    \
    -DBUILD_MOCKTRACER=OFF \
    -DBUILD_SHARED_LIBS=ON \
    -DBUILD_STATIC_LIBS=ON \
    -DBUILD_DYNAMIC_LOADING=ON \
    -DCMAKE_INSTALL_PREFIX=%{buildroot}%{haproxy_lib} \
    ..
  make
  make install
popd

pushd $RPM_BUILD_DIR/libs/opentracing-c-wrapper-%{ot_c_wrapper_ver}
  ./configure \
    --prefix=%{buildroot}%{haproxy_lib} \
    --with-opentracing=%{buildroot}%{haproxy_lib} 
  sed -i 's|src test|src|' Makefile
  make 
  make install
  mkdir -p %{buildroot}%{haproxy_lib}/include/opentracing-c-wrapper-1-1-0/include/opentracing-c-wrapper
  #touch %{buildroot}%{haproxy_lib}/include/opentracing-c-wrapper-1-1-0/include/opentracing-c-wrapper/
  #cp $RPM_BUILD_DIR/libs/opentracing-c-wrapper-%{ot_c_wrapper_ver}/include/*.h\
  #   %{buildroot}%{haproxy_lib}/include/opentracing-c-wrapper-1-1-0/include/
  cp $RPM_BUILD_DIR/libs/opentracing-c-wrapper-%{ot_c_wrapper_ver}/include/opentracing-c-wrapper/*.h \
     %{buildroot}%{haproxy_lib}/include/opentracing-c-wrapper-1-1-0/include/opentracing-c-wrapper/
popd

pushd $RPM_BUILD_DIR/libs/lua-5.3.5
make linux
make INSTALL_TOP=$RPM_BUILD_DIR/libs/ install
popd

pushd $RPM_BUILD_DIR/libs/openssl-%{openssl_ver}
export STATICLIBSSL=$RPM_BUILD_DIR/libs/staticlibssl
./config --prefix=$STATICLIBSSL no-shared
make && make install_sw
popd

pushd $RPM_BUILD_DIR/libs/pcre-8.42
./configure --disable-shared --disable-cpp --enable-jit --prefix=$RPM_BUILD_DIR/libs/
make
make install
popd



%ifarch %ix86 x86_64
regparm_opts="USE_REGPARM=1"
%endif


make %{?_smp_mflags} \
    TARGET="linux-glibc-legacy" \
    USE_OT=1 \
    OT_INC=%{buildroot}%{haproxy_lib}/include/opentracing-c-wrapper-1-1-0/include \
    OT_LIB=%{buildroot}%{haproxy_lib}/lib/ \
    OT_RUNPATH=%{haproxy_lib} \
    USE_OPENSSL=1 \
    SSL_INC=$RPM_BUILD_DIR/libs/staticlibssl/include \
    SSL_LIB=$RPM_BUILD_DIR/libs/staticlibssl/lib     \
    USE_PCRE=1 \
    USE_ZLIB=1 \
    USE_VSYSCALL=1 \
    USE_STATIC_PCRE=1 \
    USE_PCRE_JIT=1 \
    USE_GETADDRINFO=1 \
    USE_DL=1 \
    USE_PROMEX=1 \
    USE_QUIC=    \
    USE_NS=  \
%if %{with lua}
    USE_LUA=1 \
    LUA_LIB_NAME=lua \
    LUA_LIB=$RPM_BUILD_DIR/libs/lib     \
    LUA_INC=$RPM_BUILD_DIR/libs/include \
%endif
    ${regparm_opts} \
    ADDINC="%{optflags} " \
    USE_LINUX_TPROXY=1 \
    ADDLIB="%{?__global_ldflags} -lpthread"
    #ADDLIB="%{?__global_ldflags} -L$RPM_BUILD_DIR/libs/lib -lpthread"

patchelf --set-rpath %{haproxy_lib}/lib haproxy

%{__make} TARGET="linux-glibc-legacy" OPTIMIZE="%{optflags}" admin/halog/halog

pushd admin/iprange
%{__make} iprange OPTIMIZE="%{optflags}"
popd

%install
#rm -rf %{buildroot}

%{__make} install-bin DESTDIR=%{buildroot} PREFIX=%{_prefix} TARGET="linux-glibc-legacy"
%{__make} install-man DESTDIR=%{buildroot} PREFIX=%{_prefix}

%if %{with systemd}
%{__install} -p -D -m 0644 %{SOURCE6} %{buildroot}%{_unitdir}/haproxy.service
%else
%{__install} -p -D -m 0755 %{SOURCE1} %{buildroot}%{_initrddir}/haproxy
%endif

%{__install} -p -D -m 0644 %{SOURCE2} %{buildroot}%{haproxy_confdir}/%{name}.cfg
%{__install} -p -D -m 0644 %{SOURCE3} %{buildroot}%{_sysconfdir}/logrotate.d/%{name}
%{__install} -p -D -m 0644 %{SOURCE4} %{buildroot}%{_sysconfdir}/sysconfig/haproxy
%{__install} -p -D -m 0644 %{SOURCE5} %{buildroot}%{_mandir}/man1/halog.1
%{__install} -p -D -m 0644 %{SOURCE7} %{buildroot}%{haproxy_confdir}/jaeger-config.json

%{__install} -d -m 0755 %{buildroot}%{haproxy_home}
%{__install} -d -m 0755 %{buildroot}%{haproxy_datadir}
%{__install} -d -m 0755 %{buildroot}%{_bindir}
%{__install} -p -m 0755 ./admin/halog/halog     %{buildroot}%{_bindir}/halog
%{__install} -p -m 0644 ./examples/errorfiles/* %{buildroot}%{haproxy_datadir}

for file in $(find . -type f -name '*.txt') ; do
    iconv -f ISO-8859-1 -t UTF-8 -o $file.new $file && \
    touch -r $file $file.new && \
    mv $file.new $file
done

%{__install} -d -m 0755 %{buildroot}%{haproxy_lib}
%{__install} -d -m 0755 %{buildroot}%{haproxy_mod}

install -p -D -m 0755 \
  $RPM_BUILD_DIR/libs/jaeger-client-cpp-%{jaeger_cpp_ver}/build.d/libjaegertracing_plugin.so \
  %{buildroot}%{haproxy_mod}/libjaegertracing_plugin.so

pushd $RPM_BUILD_DIR/libs/opentracing-cpp-%{ot_cpp_ver}/build.d
make install
popd

pushd $RPM_BUILD_DIR/libs/opentracing-c-wrapper-%{ot_c_wrapper_ver}
make install
popd

patchelf --set-rpath /var/lib/haproxy/lib/ %{buildroot}%{haproxy_lib}/lib/libopentracing-c-wrapper.so.0.1.0

rm -rf %{buildroot}%{haproxy_lib}/include
rm -rf %{buildroot}%{haproxy_lib}/lib/cmake
rm -rf %{buildroot}%{haproxy_lib}/lib/pkgconfig
rm -rf %{buildroot}%{haproxy_lib}/lib/*.a
rm -rf %{buildroot}%{haproxy_lib}/lib/*.la
rm -f  %{buildroot}%{haproxy_lib}/lib/*/*.o

export QA_SKIP_BUILD_ROOT=1
export QA_RPATHS=$[ 0x0002 ]

%clean
rm -rf %{buildroot}

%pre
%{_sbindir}/groupadd -g %{haproxy_gid} -r %{haproxy_group} 2>/dev/null || :
%{_sbindir}/useradd -u %{haproxy_uid} -g %{haproxy_group} -d %{haproxy_home} -s /sbin/nologin -r %{haproxy_user} 2>/dev/null || :

%post
/sbin/chkconfig --add haproxy

%preun
if [ "$1" -eq 0 ]; then
    /sbin/service haproxy stop >/dev/null 2>&1
    /sbin/chkconfig --del haproxy
fi

%postun
if [ "$1" -ge 1 ]; then
    /sbin/service haproxy condrestart >/dev/null 2>&1 || :
fi

%files
%defattr(-,root,root,-)
%defattr(-,root,root)
%doc CHANGELOG README doc/architecture.txt doc/configuration.txt doc/intro.txt doc/management.txt doc/proxy-protocol.txt
%doc examples/acl-content-sw.cfg
%doc examples/content-sw-sample.cfg
%doc examples/option-http_proxy.cfg
%doc examples/socks4.cfg
%doc examples/transparent_proxy.cfg
%doc examples/wurfl-example.cfg
%doc examples/haproxy.init
%doc %{_mandir}/man1/%{name}.1*

%{haproxy_datadir}
%dir %{haproxy_confdir}
%config(noreplace) %{haproxy_confdir}/%{name}.cfg
%config(noreplace) %{haproxy_confdir}/jaeger-config.json
%config(noreplace) %{_sysconfdir}/logrotate.d/%{name}
%config(noreplace) %{_sysconfdir}/sysconfig/%{name}
%attr(0755,root,root) %config %{_sysconfdir}/rc.d/init.d/%{name}
%attr(0755,root,root) %{_sbindir}/%{name}
%{_bindir}/halog
%{_mandir}/man1/%{name}.1.gz
%{_mandir}/man1/halog.1.gz
%attr(-,%{haproxy_user},%{haproxy_group}) %dir %{haproxy_home}
#%exclude %{_sbindir}/haproxy-systemd-wrapper

                 %dir %{haproxy_lib}
                 %dir %{haproxy_lib}/lib
%attr(0755,root,root) %{haproxy_lib}/lib/*
                 %dir %{haproxy_mod}
%attr(0755,root,root) %{haproxy_mod}/libjaegertracing_plugin.so

%changelog
